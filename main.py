#! /usr/bin/env python3

import gaydetector.db as db
import gaydetector.bot as bot


def main():
    db.create_db()
    bot.bot.polling()


if __name__ == '__main__':
    main()
