import json
import random

import telebot
import telebot.apihelper

import gaydetector.db as db


dotenv = json.load(open('.env.json'))

bot = telebot.TeleBot(dotenv['token'], threaded=False)
admin_id = dotenv['admin_id']
if 'proxy' in dotenv and dotenv['proxy'] is not None:
    telebot.apihelper.proxy = {'https': dotenv['proxy']}


def get_readable_name(user):
    name = user.first_name
    if hasattr(user, 'second_name'):
        name += ' ' + user.second_name
    return name


def get_readable_title(chat):
    if hasattr(chat, 'title'):
        return chat.title
    return 'id=' + str(chat.id)


def check_admin(message):
    if message.from_user.id != admin_id:
        bot.reply_to(message, 'Sorry, only admin can use this command :(')
        return False
    return True



@bot.message_handler(commands=['register'])
def register_user(message):
    is_registered = db.register_user(message.chat.id, message.from_user.id)
    if is_registered:
        format_string = '{name}, you are already registered'
    else:
        format_string = '{name}, you are successfully registered!'
    name = get_readable_name(message.from_user)
    bot.reply_to(message, format_string.format(name=name))


@bot.message_handler(commands=['unregister'])
def unregister_user(message):
    is_registered = db.unregister_user(message.chat.id, message.from_user.id)
    if is_registered:
        format_string = '{name}, you are successfully unregistered!'
    else:
        format_string = '{name}, you were not registered!'
    name = get_readable_name(message.from_user)
    bot.reply_to(message, format_string.format(name=name))


@bot.message_handler(commands=['list_registered'])
def list_registered(message):
    registered_users = db.list_registered(chat_id=message.chat.id)
    reply = '\n'.join(
        get_readable_name(bot.get_chat_member(chat_id=message.chat.id, user_id=user_id).user)
        for user_id in registered_users
    ) or 'Noone is registered :('
    bot.reply_to(message, reply)


@bot.message_handler(commands=['gay_of_the_day'])
def gay_of_the_day(message):
    gay_id, is_already_chosen = db.choose_current_gay(message.chat.id)
    if is_already_chosen:
        format_string = "Today's gay of the day is {gay_name}"
    else:
        format_string = "{gay_name} is now offically gay of the day!"
    gay = bot.get_chat_member(chat_id=message.chat.id, user_id=gay_id).user if gay_id else None
    gay_name = get_readable_name(gay) if gay else None
    noone_reg = 'Noone is registered, cannot choose Gay of the day :('
    bot.send_message(message.chat.id, format_string.format(gay_name=gay_name) if gay_name else noone_reg)


@bot.message_handler(commands=['set_timezone'])
def set_timezone(message):
    words = message.text.split(' ')
    tzname = (words + [''])[1]
    db.set_timezone(message.chat.id, tzname)
    bot.reply_to(message, 'Timezone was successfully set to {tzname}'.format(tzname=tzname))


@bot.message_handler(commands=['reset_gay'])
def reset_gay(message):
    if not check_admin(message):
        return
    db.reset_gay(message.chat.id)
    bot.send_message(admin_id, 'Gay in {} is successfully reset'.format(get_readable_title(message.chat)))


@bot.message_handler(commands=['unregister_all'])
def unregister_all(message):
    if not check_admin():
        return
    db.unregister_all(message.chat.id)
    bot.send_message(admin_id, 'Successfully unregistered all members of {}'.format(get_readable_title(chat)))
