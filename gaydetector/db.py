import sqlite3
import datetime
import random

import dateutil.tz

db = sqlite3.connect('gaydetector.db', detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)


def create_db():
    db.execute('''CREATE TABLE IF NOT EXISTS "registered_users"
    (user_id INTEGER, chat_id INTEGER)''')
    db.execute('''CREATE TABLE IF NOT EXISTS "chats"
    (chat_id INTEGER PRIMARY KEY, last_chosen DATE, user_id INTEGER, tz TEXT) WITHOUT ROWID''')
    db.commit()


def check_if_registered(chat_id, user_id):
    c = db.cursor()
    c.execute('''SELECT * FROM "registered_users" WHERE chat_id=:chat_id AND user_id=:user_id''', {
        'chat_id': chat_id, 'user_id': user_id,
    })
    user = c.fetchone()
    return user is not None


def register_user(chat_id, user_id):
    if check_if_registered(chat_id, user_id):
        return True
    db.execute('''INSERT INTO "registered_users" (user_id, chat_id) VALUES (:user_id, :chat_id)''', {
        'chat_id': chat_id, 'user_id': user_id,
    })
    db.commit()
    return False


def unregister_user(chat_id, user_id):
    if not check_if_registered(chat_id, user_id):
        return False
    db.execute('''DELETE FROM "registered_users" WHERE user_id=:user_id AND chat_id=:chat_id''', {
        'chat_id': chat_id, 'user_id': user_id,
    })
    db.commit()
    return True


def less_then_today(chat_id):
    now_server = datetime.datetime.now(dateutil.tz.gettz())
    c = db.cursor()
    c.execute('''SELECT last_chosen, tz FROM "chats" WHERE chat_id=:chat_id''', {'chat_id': chat_id})
    row = c.fetchone()
    if row is None or row[0] is None:
        return True
    client_tz = dateutil.tz.gettz(row[1])
    date = row[0]
    now_client = now_server.astimezone(client_tz).date()
    return date < now_client


def list_registered(chat_id):
    return [user_id for user_id, _ in
             db.execute('''SELECT * FROM "registered_users" WHERE chat_id=:chat_id''', {'chat_id':chat_id})]


def insert_chat_if_not_already(chat_id):
    db.execute('''INSERT OR IGNORE INTO "chats" (chat_id) VALUES (:chat_id)''', {'chat_id': chat_id})


def choose_current_gay(chat_id):
    c = db.cursor()
    c.execute('''SELECT * FROM "chats" WHERE chat_id=:chat_id''', {'chat_id': chat_id})
    chat = c.fetchone()
    if not less_then_today(chat_id):
        return chat[2], True
    insert_chat_if_not_already(chat_id)

    c.execute('''SELECT tz FROM "chats" WHERE chat_id=:chat_id''', {'chat_id': chat_id})
    tzname, = c.fetchone()

    users = list_registered(chat_id)
    if not users:
        return None, False
    gay_id = random.choice(users)

    if not chat:
        c.execute('''INSERT INTO "chats" (chat_id) VALUES (:chat_id)''', {'chat_id': chat_id})

    client_tz = dateutil.tz.gettz(tzname)
    now = datetime.datetime.now(dateutil.tz.gettz()).astimezone(client_tz).date()
    c.execute('''UPDATE "chats" SET (last_chosen, user_id) = (:now, :gay_id)
                 WHERE chat_id=:chat_id''', {
                     'now': now,
                     'gay_id': gay_id,
                     'chat_id': chat_id,
                 }
    )
    db.commit()
    return gay_id, False


def set_timezone(chat_id, tzname):
    insert_chat_if_not_already(chat_id)
    db.execute('''UPDATE "chats" SET tz = (:tzname) WHERE chat_id=:chat_id''', {
        'tzname': tzname,
        'chat_id': chat_id,
    })
    db.commit()


def reset_gay(chat_id):
    db.execute('''UPDATE "chats" SET (last_chosen, user_id) = (:last_chosen, :user_id)''', {
        'last_chosen': None,
        'user_id': None,
    })
    db.commit()


def unregister_all(chat_id):
    db.execute('''DELETE FROM "registered_users" WHERE chat_id=:chat_id''', {
        'chat_id': chat_id,
    })
    db.commit()
